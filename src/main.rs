use std::fmt;

fn main() {
    let mut buffer = String::new();
    let buf = &mut buffer;
    let stdin = std::io::stdin();
    let cases = {
        buf.clear();
        stdin.read_line(buf).unwrap();
        buf.trim().parse().unwrap()
    };
    for _ in 0..cases {
        let (gorilla_left, gorilla_right) = {
            buf.clear();
            stdin.read_line(buf).unwrap();
            let mut iter = buf.split_whitespace().map(|coord| coord.parse().unwrap());
            /* Or
            let mut iter = buf
                .split_whitespace()
                .map(std::str::FromStr::from_str)
                .filter_map(Result::ok);
            */
            (
                Point::new(iter.next().unwrap(), iter.next().unwrap()),
                Point::new(iter.next().unwrap(), iter.next().unwrap()),
            )
        };
        let obstacles = {
            buf.clear();
            stdin.read_line(buf).unwrap();
            buf.trim().parse().unwrap()
        };
        if obstacles == 0 {
            println!("{}", gorilla_left.y.max(gorilla_right.y) - 1);
            continue;
        }
        let mut curve = Curve {
            a: 0.0,
            b: 0.0,
            c: 0.0,
        };
        for _ in 0..obstacles {
            let obstacle = {
                buf.clear();
                stdin.read_line(buf).unwrap();
                let mut iter = buf.split_whitespace().map(|coord| coord.parse().unwrap());
                Point::new(iter.next().unwrap(), iter.next().unwrap())
            };
            if curve.y(obstacle.x as f64) < obstacle.y as f64 {
                curve = Curve::solve_points(&[gorilla_left, obstacle, gorilla_right]);
            }
        }
        let max = {
            let zeroes = curve.zero();
            let found = (zeroes.0 + zeroes.1) / 2.0;
            let found_y = curve.y(found) - 1.0;
            if found < gorilla_left.x as f64
                || found > gorilla_right.x as f64
                || found_y < gorilla_left.y as f64
                || found_y < gorilla_right.y as f64
                || found_y.is_nan()
            {
                (gorilla_left.y.max(gorilla_right.y) - 1) as f64
            } else {
                found_y
            }
        };
        println!("{:.10}", max);
    }
}

#[derive(Clone, Copy, Debug)]
struct Curve {
    a: f64,
    b: f64,
    c: f64,
}

impl fmt::Display for Curve {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}x² + {}x + {}", self.a, self.b, self.c)
    }
}

#[derive(Clone, Copy, Debug)]
struct Point {
    x: i64,
    y: i64,
}

impl Point {
    fn new(x: i64, y: i64) -> Self {
        Point {
            x: (x + 1),
            y: (y + 1),
        }
    }
}

impl Curve {
    fn zero(&self) -> (f64, f64) {
        let first = self.b / -(2.0 * self.a);
        let second = ((first.powi(2) / 4.0) - (self.b / self.a)).sqrt();
        (first + second, first - second)
    }

    fn y(&self, x: f64) -> f64 {
        self.a * x * x + self.b * x + self.c
    }

    #[allow(clippy::many_single_char_names)]
    #[allow(non_snake_case)]
    fn solve_points(points: &[Point; 3]) -> Curve {
        let a = points[0].x;
        let b = points[0].y;
        let c = points[1].x;
        let d = points[1].y;
        let e = points[2].x;
        let f = points[2].y;

        let aa = a * a;
        let cc = c * c;
        let ee = e * e;

        let aaa = a * a * a;
        let ccc = c * c * c;

        //magic from wolfram alpha
        let L1 = (cc * f - b * cc + b * c * a + d * c * a + e * b * c - e * d * c - 2 * c * a * f
            + e * d * a
            + aa * f
            - e * b * a
            - d * aa) as f64
            / ((a - c) * (e * cc - cc * a + c * aa - ee * c + ee * a - e * aa)) as f64;
        let L2 = (b * ccc - ccc * f + cc * a * f - b * cc * a + ee * d * c + c * aa * f
            - d * c * aa
            - ee * b * c
            + d * aaa
            + ee * b * a
            - ee * d * a
            - aaa * f) as f64
            / ((e * cc - cc * a + c * aa - ee * c + ee * a - e * aa) * (a - c)) as f64;
        let L3 = (b * c * (c - e) * e + a * d * e * (-a + e) + a * (a - c) * c * f) as f64
            / ((a - c) * (a - e) * (c - e)) as f64;

        let curve = Curve {
            a: L1,
            b: L2,
            c: L3,
        };

        #[cfg(debug_assertations)]
        {
            for point in points.iter() {
                let height = curve.y(point.x as f64);
                if !(height / 1.00001 < point.y as f64 && height * 1.00001 > point.y as f64) {
                    panic!("height wrong");
                }
            }
        }

        curve
    }
}
