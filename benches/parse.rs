#![feature(str_strip)]

use criterion::{BenchmarkId, Criterion, criterion_group, criterion_main};

fn std_parse(input: &str) -> i64 {
	input.parse().unwrap()
}

fn std_parse_default(input: &str) -> i64 {
	input.parse().unwrap_or_default()
}

fn manual(input: &str) -> i64 {
	input
		.as_bytes()
		.iter()
		.fold(0, |acc, &curr| acc * 10 + (curr - b'0') as i64)
}

fn bench(c: &mut Criterion) {
	let mut group = c.benchmark_group("Parse");
	for i in ["1", "50", "999", "9999", "9999999999"].iter() {
		group.bench_with_input(BenchmarkId::new("Standard parse", i), i,
		                       |b, i| b.iter(|| std_parse(i)));
		group.bench_with_input(BenchmarkId::new("Standard parse default", i), i,
		                       |b, i| b.iter(|| std_parse_default(i)));
		group.bench_with_input(BenchmarkId::new("Manual", i), i,
		                       |b, i| b.iter(|| manual(i)));
	}
	group.finish();
}

criterion_group!(benches, bench);
criterion_main!(benches);
