#![feature(str_strip)]

use criterion::{BenchmarkId, Criterion, criterion_group, criterion_main};

fn std_trim(input: &str) -> &str {
	input.trim()
}

fn std_trim_end(input: &str) -> &str {
	input.trim_end()
}

fn strip_suffix_incorrect(input: &str) -> &str {
	input.strip_suffix(char::is_whitespace).unwrap_or_default()
}

fn strip_suffix(input: &str) -> &str {
	input.strip_suffix(char::is_whitespace).unwrap_or(input)
}

fn hardcoded_trim_win(input: &str) -> &str {
	&input[..input.len() - 2]
}

fn hardcoded_trim_unix(input: &str) -> &str {
	&input[..input.len() - 1]
}

// From std::io::Lines
fn lines(mut input: &str) -> &str {
	if &input[input.len() - 1..] == "\n" {
		input = &input[..input.len() - 1];
		if &input[input.len() - 1..] == "\r" {
			input = &input[..input.len() - 1];
		}
	}
	input
}

fn bench(c: &mut Criterion) {
	let mut group = c.benchmark_group("String trim");
	for i in [("win", "50\r\n"), ("unix", "50\n")].iter() {
		group.bench_with_input(BenchmarkId::new("Standard trim", i.0), i.1,
		                       |b, i| b.iter(|| std_trim(i)));
		group.bench_with_input(BenchmarkId::new("Standard trim end", i.0), i.1,
		                       |b, i| b.iter(|| std_trim_end(i)));
		group.bench_with_input(BenchmarkId::new("Standard strip suffix incorrect", i.0), i.1,
		                       |b, i| b.iter(|| strip_suffix_incorrect(i)));
		group.bench_with_input(BenchmarkId::new("Standard strip suffix", i.0), i.1,
		                       |b, i| b.iter(|| strip_suffix(i)));
		group.bench_with_input(BenchmarkId::new("std::io::Lines algo", i.0), i.1,
		                       |b, i| b.iter(|| lines(i)));
		if i.0 == "win" {
			group.bench_with_input(BenchmarkId::new("Hardcoded trim", i.0), i.1,
			                       |b, i| b.iter(|| hardcoded_trim_win(i)));
		} else {
			group.bench_with_input(BenchmarkId::new("Hardcoded trim", i.0), i.1,
			                       |b, i| b.iter(|| hardcoded_trim_unix(i)));
		}
	}
	group.finish();
}

criterion_group!(benches, bench);
criterion_main!(benches);
